package vays;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TableConsole {

    private String clHead = ConsoleColors.YELLOW_BOLD;
    private String clBody = ConsoleColors.BLACK;
    private String clBorder = ConsoleColors.WHITE;

    //Конструктивные символы
    private char borderCharHead = '=';
    private char borderCharH = '-';
    private char borderCharV = '|';

    private String[] head;
    private List<String[]> body = new ArrayList<>();

    public TableConsole(ResultSet rs) throws SQLException {
        ResultSetMetaData mdrs = rs.getMetaData();
        int countColumn = mdrs.getColumnCount();

        //head
        head = new String[countColumn];
        for (int i = 1; i <= countColumn; i++) {
            head[i-1] = mdrs.getColumnName(i);
        }

        //body
        while (rs.next()) {
            String[] row = new String[countColumn];
            body.add(row);
            for(int i = 0; i < countColumn; i++) {
                row[i] = rs.getString(i+1);
            }
        }
    }

    public String generate() {
        StringBuilder result = new StringBuilder();

        //Вычисление максимальной ширины полей таблицы
        int arrayMaxWidth[] = new int[head.length];
        int value = 0;
        for (int i=0; i<head.length; i++) {
            value = (head[i] != null) ? head[i].length() : 0;
            if (arrayMaxWidth[i] < value) {
                arrayMaxWidth[i] = value;
            }
        }
        for (String[] row : body) {
            for(int i=0; i<row.length; i++) {
                value = (row[i] != null) ? row[i].length() : 0;
                if (arrayMaxWidth[i] < value) {
                    arrayMaxWidth[i] = value;
                }
            }
        }

        //Вычисление ширины таблицы
        int widthTable = 1;
        for(int width : arrayMaxWidth) {
            widthTable += width + 3;
        }

        //head
        char[] headBorderTemp = new char[widthTable];
        Arrays.fill(headBorderTemp, borderCharHead);
        String headBorder = new String(headBorderTemp);
        result.append(clBorder + headBorder + "\n");

        StringBuilder line = new StringBuilder();
        for(int i=0; i < head.length; i++) {
            line.append(clBorder + borderCharV + " " + clHead + String.format("%-" + arrayMaxWidth[i] + "s", head[i]) + " ");
        }
        line.append(clBorder + borderCharV);
        result.append(line + "\n");
        result.append(clBorder + headBorder + "\n");

        //body
        for(String[] row : body) {
            line = new StringBuilder();
            for(int i = 0; i < head.length; i++) {
                line.append(clBorder + borderCharV + " " + clBody + String.format("%-" + arrayMaxWidth[i] + "s", row[i]) + " ");
            }
            result.append(line + clBorder + String.valueOf(borderCharV) + "\n");
        }


        char[] bodyBorderTemp = new char[widthTable];
        Arrays.fill(bodyBorderTemp, borderCharH);
        result.append(clBorder + new String(bodyBorderTemp) + "\n");


        return result.toString();
    }
}

